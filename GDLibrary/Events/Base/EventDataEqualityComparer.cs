﻿using GDLibrary.Actors.Base;
using GDLibrary.Events.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GDLibrary.Events.Base
{
    //used by the EventDispatcher to compare events in a HashSet
    //The HashSet stops events being added to the set twice
    public class EventDataEqualityComparer : IEqualityComparer<EventData>
    {
        public bool Equals(EventData e1, EventData e2)
        {
            bool bEquals = true;

            //sometimes we don't specify ID or sender so run a test
            if (e1.ID != null && e2.ID != null)
                bEquals = e1.ID.Equals(e2.ID);

            bEquals = bEquals && e1.EventType.Equals(e2.EventType)
                    && e1.EventCategoryType.Equals(e2.EventCategoryType);

            if (e1.Sender != null && e2.Sender != null)
            { 
                bEquals = bEquals && (e1.Sender).Equals(e2.Sender);
            }

            return bEquals;
        }

        public int GetHashCode(EventData e)
        {
            return e.GetHashCode();
        }
    }
}
