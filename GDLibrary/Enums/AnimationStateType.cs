﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GDLibrary.Enums
{
    public enum AnimationStateType : sbyte
    {
        Idle,
        Running,
        RunningForward,
        RunningBackward,
        TurningLeft,
        TurningRight,
        Jumping,
        Crouching,
        AttackingPrimary,
        AttackingSecondary,
        DefendingPrimary,
        DefendingSecondary,
        Falling,
        Dying,
        Winning,
        Losing,
        Bored,
        Taunting,
        Walking
        //add all the states that you have animations for here...
    }
}
