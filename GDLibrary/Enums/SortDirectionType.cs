﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GDLibrary.Enums
{
    public enum SortDirectionType : sbyte
    {
        Ascending,
        Descending
    }
}
