﻿/* Store, update and darw all visible UI objects based on PausableDrawableGameComponent */
using GDLibrary.Actors.Base;
using GDLibrary.Enums;
using GDLibrary.Events.Base;
using GDLibrary.Templates;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GDLibrary.Events.Data;
using GDLibrary.Actors.Drawn._2D.UI;

namespace GDLibrary.Managers.UI
{
    public class UIManager : PausableDrawableGameComponent
    {
        #region Variables
        private List<Actor2D> drawList, removeList;
        private SpriteBatch spriteBatch;
        #endregion

        #region Properties
        #endregion

        public UIManager(Game game, SpriteBatch spriteBatch, EventDispatcher eventDispatcher, int initialSize, StatusType statusType)
            :base(game, eventDispatcher, statusType)
        {
            this.spriteBatch = spriteBatch;

            this.drawList = new List<Actor2D>(initialSize);
            this.removeList = new List<Actor2D>(initialSize);
        }

        //See MenuManager::EventDispatcher_MenuChanged to see how this does the reverse
        protected override void EventDispatcher_MenuChanged(EventData eventData)
        {
            //did the event come from the main menu and is it a start game event
            if(eventData.EventType == EventActionType.OnStart)
            {
                //turn on update and draw
                this.StatusType = StatusType.Update | StatusType.Drawn;
            }
            //did the event come from the main menu and is it a game pause
            else if(eventData.EventType == EventActionType.OnPause)
            {
                //turn off update and draw
                this.StatusType = StatusType.Off;
            }
        }

        public void Add(Actor2D actor)
        {
            this.drawList.Add(actor);
        }

        //call when we want to remove an element of the UI e.g player health displayed as hearts
        public void Remove(Actor2D actor)
        {
            this.removeList.Add(actor);
        }

        public int Remove(Predicate<Actor2D> predicate)
        {
            List<Actor2D> resultList = null;

            resultList = this.drawList.FindAll(predicate);
            if((resultList != null) && (resultList.Count != 0))
            {
                foreach(Actor2D actor in resultList)
                {
                    this.removeList.Add(actor);
                }
            }
            return resultList != null ? resultList.Count : 0;        
        }

        //batch remove 
        protected virtual void ApplyRemove()
        {
            foreach(Actor2D actor in this.removeList)
            {
                this.drawList.Remove(actor);
            }
            this.removeList.Clear();
        }

        protected override void ApplyUpdate(GameTime gameTime)
        {
            //remove any outstanding elements
            ApplyRemove();

            foreach(Actor2D actor in this.drawList)
            {
                if((actor.StatusType & StatusType.Update) == StatusType.Update)
                {
                    actor.Update(gameTime);
                }
            }
        }

        protected override void ApplyDraw(GameTime gameTime)
        {
            this.spriteBatch.Begin(SpriteSortMode.BackToFront, BlendState.AlphaBlend);
            foreach(Actor2D actor in this.drawList)
            {
                if ((actor.StatusType & StatusType.Drawn) == StatusType.Drawn)
                {
                    actor.Draw(gameTime, spriteBatch);
                }
            }
            this.spriteBatch.End();
            
        }



    }
}
