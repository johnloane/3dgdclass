﻿using GDLibrary.Actors.Camera;
using GDLibrary.Enums;
using GDLibrary.Events.Base;
using GDLibrary.Events.Data;
using GDLibrary.Managers.Camera;
using GDLibrary.Managers.Input;
using GDLibrary.Managers.Object;
using GDLibrary.Templates;
using GDLibrary.Utility;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GDLibrary.Managers.Screen
{
    public class ScreenManager : PausableDrawableGameComponent
    {
        #region Variables
        private ScreenUtility.ScreenType screenType;
        private ObjectManager objectManager;
        private CameraManager cameraManager;

        private KeyboardManager keyboardManager;
        private Keys pauseKey;

        private bool bFirstTime = true;
        private GraphicsDeviceManager graphics;
        private Viewport fullScreenViewport;
        #endregion

        #region Properties
        public Integer2 ScreenResolution
        {
            get
            {
                return new Integer2(this.graphics.PreferredBackBufferWidth,
                    this.graphics.PreferredBackBufferHeight);
            }
            set
            {
                graphics.PreferredBackBufferWidth = value.X;
                graphics.PreferredBackBufferHeight = value.Y;
                graphics.ApplyChanges();
            }
        }

        public ScreenUtility.ScreenType ScreenType
        {
            get
            {
                return this.screenType;
            }
            set
            {
                this.screenType = value;
            }
        }

        public Viewport FullScreenViewport
        {
            get
            {
                return fullScreenViewport;
            }
        }
        #endregion

        //public ScreenManager(Game game)
        //    :base(game)
        //{

        //}

        public ScreenManager(Game game, GraphicsDeviceManager graphics, Integer2 screenResolution,
            ScreenUtility.ScreenType screenType, ObjectManager objectManager, CameraManager cameraManager, 
            KeyboardManager keyboardManager, Keys pauseKey, EventDispatcher eventDispatcher,
            StatusType statusType)
            : base(game, eventDispatcher, statusType)
        {
            this.screenType = screenType;
            this.objectManager = objectManager;
            this.cameraManager = cameraManager;

            this.keyboardManager = keyboardManager;
            this.pauseKey = pauseKey;

            this.graphics = graphics;
            this.ScreenResolution = screenResolution;
            this.fullScreenViewport = new Viewport(0, 0, screenResolution.X, screenResolution.Y);
        }

        #region Event Handling
        /* See MenuManager::EventDispatcher_MenuChanged as this is the reverse*/
        protected override void EventDispatcher_MenuChanged(EventData eventData)
        {
            //did the event come from the main menu and is it a start event
            if (eventData.EventCategoryType == EventCategoryType.MainMenu
                && eventData.EventType == EventActionType.OnStart)
            {
                //turn off update and draw for the menu
                this.StatusType = StatusType.Update | StatusType.Drawn; 
            }
            //did the event come from the game and is it a pause game event
            else if (eventData.EventCategoryType == EventCategoryType.MainMenu
                && eventData.EventType == EventActionType.OnPause)
            {
                //turn on update and draw for the menu - the game is paused
                this.StatusType = StatusType.Off;
            }
        }
        #endregion

        public bool ToggleFullScreen()
        {
            //flip the screen mode
            this.graphics.IsFullScreen = !this.graphics.IsFullScreen;
            this.graphics.ApplyChanges();

            //return new state
            return this.graphics.IsFullScreen;
        }

        protected override void ApplyUpdate(GameTime gameTime)
        {
            //if one camera needs to be drawn on top of another then we need to do a depth sort
            //the first time the game is run
            if (this.bFirstTime && this.screenType == ScreenUtility.ScreenType.MultiPictureInPicture)
            {
                //sort so the top-most camera (closed draw depth to 0 will be last camera drawn)
                this.cameraManager.SortByDepth(SortDirectionType.Descending);
                this.bFirstTime = false;
            }

            //explicit call
            this.objectManager.Update(gameTime);
            // base.Update(gameTime);
        }

        protected override void ApplyDraw(GameTime gameTime)
        {
            if (this.screenType == ScreenUtility.ScreenType.SingleScreen)
            {
                this.objectManager.Draw(gameTime, this.cameraManager.ActiveCamera);
            }
            else
            {
                //foreach is enabled by making CameraManager implement IEnumerator
                foreach (Camera3D camera in cameraManager)
                {
                    this.objectManager.Draw(gameTime, camera);
                }
            }
            //reset the viewport to fullscreen
            this.Game.GraphicsDevice.Viewport = this.fullScreenViewport;    
        }

        protected override void HandleInput(GameTime gameTime)
        {
            #region Menu Handling
            //if the user presses the menu button then either show or hide the menu
            if(this.keyboardManager != null 
                && this.keyboardManager.IsFirstKeyPress(this.pauseKey))
            {
                //if game is paused then publish a play event
                if(this.StatusType == StatusType.Off)
                {
                    //will received by the menu manager and the screen manager
                    EventDispatcher.Publish(new EventData("SMHIPlay", this,
                        EventActionType.OnStart, EventCategoryType.MainMenu));
                }
                //if the game is playing publish a pause event
                else if(this.StatusType != StatusType.Off)
                {
                    EventDispatcher.Publish(new EventData("SMHIPause", this, EventActionType.OnPause,
                        EventCategoryType.MainMenu));
                }
            }
            #endregion

        }
    }
}
