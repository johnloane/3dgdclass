﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace GDLibrary.Utility
{
    public class StringUtility
    {
        //parse file name from a path
        public static string ParseNameFromPath(string path)
        {
            //"Assets/Textures/sky"
            return Regex.Match(path, @"[^\\/]*$").Value;

        }
    }
}
