﻿/* A stateful variable to store a succession of boolean states e.g in the menu the mouse was not over the button
 * and is over the button now, the mouse was over the button and is over the button now or the mouse was over the
 * button and is not over the button now*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GDLibrary.Utility
{
    public class StatefulBool
    {
        private int capacity;
        private List<bool> stateList;

        public StatefulBool(int capacity)
        {
            this.capacity = capacity;
            this.stateList = new List<bool>(capacity);
        }

        public void Update(bool state)
        {
            this.stateList.Insert(0, state);
            //make sure that there is always just capacity states stored
            if(this.stateList.Count > this.capacity)
            {
                this.stateList.RemoveAt(this.stateList.Count - 1);
            }
        }

        //return true if a state goes from false to true
        public bool IsActivating()
        {
            if(this.stateList.Count >= 2)
            {
                return (this.stateList[0] && !this.stateList[1]);
            }
            else
            {
                return false;
            }
        }

        //return true if a state goes from true to false
        public bool IsDeactivating()
        {
            if (this.stateList.Count >= 2)
            {
                return (!this.stateList[0] && this.stateList[1]);
            }
            else
            {
                return false;
            }
        }

        //return the last stored state
        public bool isActive()
        {
            if(this.stateList.Count >=1)
            {
                return this.stateList[0];
            }
            return false;
        }

        //return true if it continues to be active
        public bool IsStillActive()
        {
            if(this.stateList.Count > 1)
            {
                return (this.stateList[0] && this.stateList[1]);
            }
            return false;
        }

        public override string ToString()
        {
            StringBuilder str = new StringBuilder();
            foreach(bool state in this.stateList)
            {
                str.Append(state);
                str.Append(", ");
            }
            return str.ToString();
        }


    }
}
