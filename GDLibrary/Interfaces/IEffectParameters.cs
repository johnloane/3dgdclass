﻿using GDLibrary.Actors.Camera;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GDLibrary.Interfaces
{
    public interface IEffectParameters : ICloneable
    {
        void SetParameters(Camera3D camera);
        void SetWorld(Matrix world);
    }
}
